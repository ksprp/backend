## KSPRP Backend
This project will be split up into 3 different modules.

* The 'web' module handles the server.
* The 'game' module handles game logic, and is used by the server.
* The 'database' module handles the database logic, and exposes functions used in the 'game' module.

The tech stack uses sqlx for database interactions and Axum as web server, written in rust.
The database is a postgres database and is run in a separate container.

The master branch will be live at https://api.ksprp.app
The staging branch will be live at https://api.staging.ksprp.app

Please contribute by pull requests only, feel free to merge to staging to test in the live development environment (which does not work yet)
The related frontends are hosted at https://staging.ksprp.app (points to Stelar's VPS) and https://ksprp.app (points to master branch of ksprp/frontend repo)

### How to contribute

First, set up the development environment
1. copy the compose-templates/dev.docker-compose.yml file and set up a .env file in the same directory with POSTGRES_USER, POSTGRES_PASSWORD and POSTGRES_DB
2. either add a server/settings.ron file or a .env with:
KSPRP_DB_URL=postgres://POSTGRES_USER:POSTGRES_PASSWORD@localhost:5432/PSOTGRES_DB
KSPRP_BASE_URL=(set up in discord application portal)
KSPRP_DISCORD_AUTH_URL=(identify+email scopes)
KSPRP_CLIENT_ID=(make your own discord app)
KSPRP_CLIENT_SECRET=(make your own discord app)
(This will be solved automatically when deployed to staging)
3. cargo run

Let's say you wanted to add an API endpoint to read the current hex map. You would:
1. Add a new file in server/migrations where the hex table is created
2. Add methods to retrieve and set the values in the server/src/databse/ module
3. Add any game logic to the right place
  * server/src/game.rs hour_tick (called once per hour) 
  * server/src/game.rs year_tick (called once per week)
  * dedicated logic in the server/src/game/ module
4. Add a endpoint in  server/src/web/, either in the public or private modules (see existing routes for permission and auhtorization examples), also check app.rs

### What should be done?
Basically everything in API.md needs to be implemented. Currently implemented:
* /public/time

More detailed API.md is coming.
