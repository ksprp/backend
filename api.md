This will be expanded with parameters and example return values later.

# /private/admin/ Admin Interface

### POST /paint Paint Terrain type and Nation ownership for hexes
### POST /nation Add a new Nation

# /private/user/ User Interface
## /nation/
### POST /rename Rename existing Nation
### POST /projects Add new projects to the queue or modify the existing queue
### POST /military Add new orders to the next turn or modify the existing orders
### GET  /cities Cities
### GET  /military Divsisions and orders
### GET  /battles Status of involved ongoing battles
### GET  /intel Get known information about other nations military
### GET  /projects Project Queue
### GET  /tech Researched technology
### GET  /buildings Constructed buildings
### GET  /log Everything that has happened, in text format with time stamps

# /public/ Public Interface
### GET /map Get all hexes
### GET /nations Get all (public) nation information
### GET /time Current RP time
