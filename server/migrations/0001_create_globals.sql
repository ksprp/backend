create table if not exists globals (
  id serial primary key,
  name varchar unique not null,
  value integer not null 
);

insert into globals (name, value) values ('year', 1926);
insert into globals (name, value) values ('hour', 0);
