create table if not exists timestamps (
  id serial primary key,
  name varchar unique not null,
  value timestamp with time zone not null 
);

insert into timestamps (name, value) values ('last_updated', '2024-03-11:00:00.000Z');
