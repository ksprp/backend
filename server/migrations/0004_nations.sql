create type ideology_type as enum ('WO-A', 'WO-P', 'WO-D', 'P-A', 'P-P', 'P-D', 'PO-A', 'PO-P', 'PO-D');
create table if not exists nations (
    -- IDs
    id serial primary key,
    user_id text references users(id) not null,
    -- Description
    name_full text not null,
    name_short text not null,
    denonym text not null,
    adjective text not null,
    flag_url text not null,
    government_type text not null,
    leader text not null,
    ideology ideology_type not null,
    -- Mechanics
    balance numeric(10,2) default 50.00, -- in MK 10 digits max with 2 fraction digits, 99,999,999.99
    country_mod numeric(10,2) default 50.00,
    country_mod_pop_mult real not null -- Multiply country mod by this number to get population
);
