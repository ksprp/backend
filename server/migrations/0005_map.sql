create type biome as enum ('ice', 'tundra', 'highlands', 'mountanis', 'grasslands', 'deserts', 'badlands', 'shores', 'water');
create table if not exists map (
    id serial primary key,
    q integer not null,
    r integer not null,
    nation_id integer references nations(id), -- Can be NULL if not owned
    biome biome not null
);

-- To avoid having 192x84 insert calls here we set a initialized flag to 0, and set it to 1 after map init
insert into globals (name, value) values ('map_initialized', 0);
