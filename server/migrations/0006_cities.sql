create table if not exists cities (
    id serial primary key,
    nation_id integer references nations(id) not null,
    hex_id integer references map(id) not null,
    capital boolean default false,
    size integer not null -- 5 for largest city, 4 for second largest .. 1 for smallest of 5 mapped cities
);
