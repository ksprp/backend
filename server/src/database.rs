pub mod globals;
pub mod map;
pub mod time;

use sqlx::{migrate, postgres::PgPoolOptions, PgPool};
use tracing::{info, instrument};

use crate::{
    database::{
        globals::{get_global, set_global},
        map::initialize_map,
    },
    SETTINGS,
};

/// Establishes a connection with the postgres database
#[instrument]
pub async fn setup() -> Result<PgPool, sqlx::Error> {
    // Connect to database
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&SETTINGS.db_url)
        .await?;
    info!("Connected to database");

    // Run migrations
    info!("Running migrations");
    migrate!("./migrations").run(&pool).await?;

    // Check if hex map is initialized
    let mut tx = pool.begin().await?;
    if get_global(&mut tx, "map_initialized").await? == 0 {
        info!("Initializing hex map");
        initialize_map(&mut tx).await?;
        set_global(&mut tx, "map_initialized", 1).await?;
        tx.commit().await?;
    } else {
        info!("Hex map already initialized")
    }

    Ok(pool)
}
