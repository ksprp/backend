use sqlx::PgConnection;

struct Global {
    pub value: i32,
}

/// Helper to get a value from the globals key-value table
pub async fn get_global(db: &mut PgConnection, key: &str) -> Result<i32, sqlx::Error> {
    let Global { value } = sqlx::query_as!(
        Global,
        "
SELECT value
FROM globals
WHERE name = $1
        ",
        key
    )
    .fetch_one(db)
    .await?;

    Ok(value)
}

/// Helper to set a value in the globals key-value table
pub async fn set_global(db: &mut PgConnection, key: &str, value: i32) -> Result<(), sqlx::Error> {
    sqlx::query!(
        "
UPDATE globals
SET value = $2
WHERE name = $1
        ",
        key,
        value
    )
    .execute(db)
    .await?;

    Ok(())
}
