const MAP_WIDTH: i32 = 192;
const MAP_HEIGHT: i32 = 84;

use sqlx::PgConnection;
use tracing::info;

use crate::game::map::{Biome, Hex};

pub async fn initialize_map(conn: &mut PgConnection) -> Result<(), sqlx::Error> {
    info!("Initializing hex map to defaults");
    for q in 0..MAP_WIDTH {
        for r in 0..MAP_HEIGHT {
            sqlx::query!(
                "
                insert into map (q, r, biome) values ($1, $2, $3);
                ",
                q,
                r,
                Biome::Grasslands as Biome,
            )
            .execute(&mut *conn)
            .await?;
        }
    }
    info!("Done initializing hex map");

    Ok(())
}

/// Retrieve all the public map information
pub async fn get_map(conn: &mut PgConnection) -> Result<Vec<Hex>, sqlx::Error> {
    sqlx::query_as!(
        Hex,
        "
        select
        id,
        q,
        r,
        nation_id,
        biome AS \"biome: Biome\"
        from map
        "
    )
    .fetch_all(conn)
    .await
}

pub async fn set_map(conn: &mut PgConnection, updated_hexes: Vec<Hex>) -> Result<(), sqlx::Error> {
    for hex in updated_hexes {
        sqlx::query!(
            "
            insert into map (id, nation_id, biome)
            values ($1, $2, $3)
            on conflict (id) do update set
                nation_id = excluded.nation_id,
                biome = excluded.biome
            ",
            hex.id,
            hex.nation_id,
            hex.biome as Biome
        )
        .execute(&mut *conn)
        .await?;
    }
    Ok(())
}
