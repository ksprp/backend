use crate::database::globals::{get_global, set_global};
use chrono::{DateTime, Utc};
use sqlx::PgConnection;

/// Get the value of 'year' from globals
pub async fn get_year(db: &mut PgConnection) -> Result<i32, sqlx::Error> {
    get_global(db, "year").await
}

/// Sets the value of 'year' in globals
pub async fn set_year(db: &mut PgConnection, year: i32) -> Result<(), sqlx::Error> {
    set_global(db, "year", year).await
}

/// Get the value of 'hour' from globals
pub async fn get_hour(db: &mut PgConnection) -> Result<i32, sqlx::Error> {
    get_global(db, "hour").await
}

pub async fn set_hour(db: &mut PgConnection, hour: i32) -> Result<(), sqlx::Error> {
    set_global(db, "hour", hour).await
}

struct Timestamp {
    pub value: DateTime<Utc>,
}

pub async fn get_last_update(db: &mut PgConnection) -> Result<DateTime<Utc>, sqlx::Error> {
    get_timestamp(db, "last_updated").await
}

pub async fn set_last_update(
    db: &mut PgConnection,
    timestamp: DateTime<Utc>,
) -> Result<(), sqlx::Error> {
    set_timestamp(db, "last_updated", timestamp).await
}

/// Helper to get a value from the globals key-value table
async fn get_timestamp(db: &mut PgConnection, key: &str) -> Result<DateTime<Utc>, sqlx::Error> {
    let Timestamp { value } = sqlx::query_as!(
        Timestamp,
        "
SELECT value
FROM timestamps
WHERE name = $1
        ",
        key
    )
    .fetch_one(db)
    .await?;

    Ok(value)
}

async fn set_timestamp(
    db: &mut PgConnection,
    key: &str,
    value: DateTime<Utc>,
) -> Result<(), sqlx::Error> {
    sqlx::query!(
        "
UPDATE timestamps
SET value = $2
WHERE name = $1
        ",
        key,
        value
    )
    .execute(db)
    .await?;

    Ok(())
}
