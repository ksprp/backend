use std::ops::Rem;

use anyhow::Context;
use chrono::{Duration, DurationRound, TimeDelta};
use sqlx::{PgConnection, PgPool, Postgres, Transaction};
use tracing::{info, instrument};

use crate::database;

pub mod map;
pub mod nation;
pub mod time;
pub mod user;

/// Called once per minute
#[instrument]
pub async fn update(db: &PgPool) -> anyhow::Result<()> {
    // Last update should always be a whole hour, but round down in case.
    let mut last_update = database::time::get_last_update(&mut *db.acquire().await?)
        .await
        .context("Failed to get last update timestamp")?
        .duration_round(TimeDelta::hours(1))
        .context("Failed to round last update timestamp")?;
    let now = chrono::offset::Utc::now();

    let difference_hours = (now - last_update).num_hours();
    if difference_hours > 0 {
        info!("{difference_hours} hours since last update");
        // Run one hour tick at a time, at each step check if there's a new week for the weekly tick
        for i in 1..=difference_hours {
            info!("Beginning update {i}/{difference_hours} of current batch");
            let mut tx: Transaction<'_, Postgres> = db.begin().await?;
            let mut year = database::time::get_year(&mut tx).await?;
            let mut hour = database::time::get_hour(&mut tx).await?;

            info!("Time before update: {year}-{hour}");

            hour += 1;
            hour_tick(&mut tx, hour).await;

            // End of day tick
            if hour.rem(24) == 0 {
                day_tick(&mut tx, (hour / 24) as i32).await;
            }

            // New year: 7 * 24 = 168
            if hour == 168 {
                year += 1;
                hour = 0;
                week_tick(&mut tx, year).await;
            }

            last_update += Duration::hours(1);
            database::time::set_year(&mut tx, year).await?;
            database::time::set_hour(&mut tx, hour).await?;
            database::time::set_last_update(&mut tx, last_update).await?;

            info!("Time after update: {year}-{hour}");

            tx.commit().await?;
        }
    }
    Ok(())
}

/// Called once per hour
pub async fn hour_tick(_db: &mut PgConnection, new_hour: i32) {
    info!("hour_tick: hour = {}", new_hour);
}

// Called once per day
pub async fn day_tick(_db: &mut PgConnection, new_day: i32) {
    info!("day_tick: day = {}", new_day);
}

/// Called once per week
pub async fn week_tick(_db: &mut PgConnection, new_year: i32) {
    info!("week_tick: year = {}", new_year);
}
