use serde::{Deserialize, Serialize};
use sqlx::{prelude::FromRow, PgPool};
use utoipa::ToSchema;

use crate::database::map::{get_map, set_map};

/// Descripes how diffuclt the terrain is within a map
#[derive(Debug, Clone, Serialize, Deserialize, sqlx::Type, PartialEq, PartialOrd, ToSchema)]
#[sqlx(type_name = "biome", rename_all = "lowercase")]
#[serde(rename_all = "lowercase")]
pub enum Biome {
    Ice,
    Tundra,
    Highlands,
    Grasslands,
    Deserts,
    Badlands,
    Shores,
    Water,
}

#[derive(Debug, Clone, Serialize, FromRow, ToSchema)]
pub struct Hex {
    #[schema(example = 34)]
    pub id: i32,
    /// column
    #[schema(example = 0)]
    pub q: i32,
    /// row
    #[schema(example = 33)]
    pub r: i32,
    /// If owned, nation id, otherwise null
    #[schema(example = "null")]
    pub nation_id: Option<i32>,
    #[schema(example = Biome::Grassland)]
    pub biome: Biome,
}

#[derive(Debug, Clone, Serialize, Deserialize, ToSchema)]
pub struct HexUpdate {
    #[schema(example = 1)]
    pub id: i32,
    #[schema(example = "null")]
    pub nation_id: Option<i32>,
    #[schema(example = Biome::Ice)]
    pub biome: Option<Biome>,
}

/// Takes a list of hex updates and applies them
pub async fn update_map(db: PgPool, hex_updates: Vec<HexUpdate>) -> anyhow::Result<()> {
    // Start a transaction
    let mut tx = db.begin().await?;

    // For each UpdateHex, update the matching hex
    let map: Vec<Hex> = get_map(&mut tx).await?;
    let mut updated_hexes: Vec<Hex> = Vec::new();
    for hex_update in &hex_updates {
        for hex in &map {
            if hex.id == hex_update.id {
                let mut hex = hex.clone();
                if let Some(nation_id) = &hex_update.nation_id {
                    hex.nation_id = Some(nation_id.clone());
                }
                if let Some(biome) = &hex_update.biome {
                    hex.biome = biome.clone();
                }
                updated_hexes.push(hex.clone());
            }
        }
    }

    // Insert the new hexes and commit the transaction
    set_map(&mut tx, updated_hexes).await?;
    tx.commit().await?;

    Ok(())
}
