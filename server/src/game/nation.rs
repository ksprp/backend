use serde::{Deserialize, Serialize};

use crate::game::user::User;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Economic {
    WorkerOriented,
    Pragmatic,
    PropertyOriented,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Governmental {
    Authoritarian,
    Paternal,
    Democratic,
}

/// Nation compass ideology, only broadly represents actual ideologies
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Ideology {
    economic: Economic,
    governmental: Governmental,
}

/// Nation description, these can be modified by users.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Description {
    name_full: String,
    name_short: String,
    denonym: String,
    adjective: String,
    flag_url: String,
    government_type: String,
    leader: String,
    ideology: Ideology,
    /// Multiply this with country mod to get the nation population.
    country_mod_pop_mult: f32,
}

/// A player Nation
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Nation {
    id: u32,
    owner: User,
    description: Description,
    balance: f64,
    country_mod: f64,
}
