use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

#[derive(Serialize, Deserialize, ToSchema)]
pub struct Time {
    /// Ingame year
    #[schema(example = 1927)]
    pub year: i32,
    /// Hours passed since the beginning of the real world week
    #[schema(example = 25)]
    pub hour: i32,
}
