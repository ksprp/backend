use anyhow::Result;
use config::Config;
use lazy_static::lazy_static;
use serde::Deserialize;
use web::app::KSPRPApp;

mod database;
mod game;
mod web;

#[derive(Deserialize)]
pub struct Settings {
    db_url: String,
    frontend_origin: String,
}

lazy_static! {
    static ref SETTINGS: Settings = Config::builder()
        // Environment isnt used when file source is added, even when file is missing, only use settings.ron for local testing
        //.add_source(config::File::with_name("settings.ron"))
        .add_source(config::Environment::with_prefix("KSPRP"))
        .build()
        .expect("Unable to find settings")
        .try_deserialize()
        .expect("Unable to parse settings");
}

#[tokio::main]
async fn main() -> Result<()> {
    // Set up logging, _guard needs to be in main lifetime
    let file_appender = tracing_appender::rolling::daily("./logs/", "ksprp.log");
    let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);
    tracing_subscriber::fmt()
        .compact()
        .with_ansi(false)
        .with_writer(non_blocking)
        .with_env_filter(std::env::var("RUST_LOG").unwrap_or_else(|_| {
            "axum_login=debug,tower_session=debug,sqlx=warn,tower_http=debug,ksprp_backend=info"
                .into()
        }))
        .init();

    KSPRPApp::new().await?.serve().await
}
