pub mod apidoc;
pub mod app;
pub mod auth;
pub mod private;
pub mod public;
pub mod utils;

async fn root() -> &'static str {
    "Hello, World!"
}
