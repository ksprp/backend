use crate::web::private;
use crate::web::public;
use utoipa::OpenApi;

#[derive(OpenApi)]
#[openapi(
    paths(
        public::time::handler,
        public::map::handler,
        private::admin::paint::handler
    ),
    components(
        schemas(
            crate::game::time::Time,
            crate::game::map::Hex,
            crate::game::map::HexUpdate,
            crate::game::map::Biome,
        )
    ),
    tags(
        (name = "KSPRP", description = "API to access and manipulate the KSPRP Backend")
    )
)]
pub struct ApiDoc;
