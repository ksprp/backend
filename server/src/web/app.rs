use core::fmt;

use crate::{
    game, permission_required,
    web::{self, apidoc::ApiDoc, root},
};
use anyhow::Result;
use axum::{
    http::{HeaderValue, Method},
    routing::{get, options},
    Router,
};
use jsonwebtoken::DecodingKey;
use sqlx::PgPool;
use tokio::{task, time};
use tracing::{error, info, instrument};
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

use crate::{database, SETTINGS};

use tower_http::cors::{AllowHeaders, CorsLayer};

#[derive(Clone)]
pub struct KSPRPApp {
    pub db: PgPool,
    pub jwt_decoding_key: DecodingKey,
}

// Because DecodingKey does not implement Debug we need a custom impl
impl fmt::Debug for KSPRPApp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("KSPRPApp").field("db", &self.db).finish()
    }
}

impl KSPRPApp {
    #[instrument]
    pub async fn new() -> Result<Self> {
        let db = database::setup()
            .await
            .expect("Unable to connect to database");

        // If we ever have multiple signing keys or if we discover keys change over time this will break
        let jwks = web::auth::jwks::get_jwks().await?;
        let jwk = jwks.keys.first().expect("JWKS list was empty");
        let jwt_decoding_key = DecodingKey::from_jwk(jwk)?;
        Ok(Self {
            db,
            jwt_decoding_key,
        })
    }

    #[instrument]
    pub async fn serve(self) -> Result<()> {
        // This task runs game_update once per minute, which handles most game logic not triggered by users
        let periodic_update_pool = self.db.clone();
        let periodic_update = task::spawn(async move {
            let mut interval = time::interval(time::Duration::from_secs(60));

            loop {
                interval.tick().await;

                // Main game loop
                game::update(&periodic_update_pool)
                    .await
                    .map_err(|e| {
                        error!("Error in 'game::update', will rollback transaction: {}", e);
                        e
                    })
                    .unwrap();
            }
        });

        let cors_layer = CorsLayer::new()
            .allow_methods([Method::GET, Method::POST])
            .allow_headers(AllowHeaders::any())
            .allow_origin(
                SETTINGS
                    .frontend_origin
                    .clone()
                    .parse::<HeaderValue>()
                    .unwrap(),
            );

        let app = Router::new()
            .route("/", get(root))
            .merge(SwaggerUi::new("/api-docs").url("/api-docs/openapi.json", ApiDoc::openapi()))
            .nest("/public", web::public::router(self.clone()))
            .nest(
                "/private",
                web::private::router(self.clone())
                    .route_layer(permission_required!(self.clone(), Permission::User)),
            )
            .route("/", options(|| async { "OK" }))
            .route("/*path", options(|| async { "OK" }))
            .layer(cors_layer);

        // Run the app with hyper, listening globally on port 3001
        info!("Starting server");
        let listener = tokio::net::TcpListener::bind("0.0.0.0:3001").await.unwrap();
        axum::serve(listener, app).await.unwrap();
        periodic_update.await.ok();
        Ok(())
    }
}
