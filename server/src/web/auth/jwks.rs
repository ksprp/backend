use anyhow::{Context, Result};
use jsonwebtoken::jwk::JwkSet;

const JWKS_URL: &'static str = "https://auth.ksprp.app/application/o/ksprp/jwks/";

pub async fn get_jwks() -> Result<JwkSet> {
    let response = reqwest::get(JWKS_URL).await?.json().await?;
    Ok(serde_json::from_value(response).context("Failed to parse JSON data as JWKS")?)
}
