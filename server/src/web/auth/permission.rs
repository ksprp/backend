use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum Permission {
    #[serde(rename = "users")]
    User,
    #[serde(rename = "moderators")]
    Moderator,
    #[serde(rename = "admins")]
    Admin,
    #[serde(other)]
    /// DO NOT USE
    Other,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TokenClaims {
    pub sub: String,
    pub iat: usize,
    pub exp: usize,
    pub groups: Vec<Permission>,
    pub name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserId {
    pub id: String,
    pub roles: Vec<Permission>,
}

#[macro_export]
macro_rules! permission_required {
    ($state:expr, $perm:expr) => {{
        use crate::web::auth::permission::*;
        use axum::extract::{Request, State};
        use axum::http::{header, StatusCode};
        use axum::middleware::{self, Next};
        use axum::response::IntoResponse;

        middleware::from_fn_with_state(
            $state,
            |State(KSPRPApp {
                 db,
                 jwt_decoding_key,
             }): State<KSPRPApp>,
             mut req: Request,
             next: Next| async move {
                let token = req
                    .headers()
                    .get(header::AUTHORIZATION)
                    .and_then(|auth_header| auth_header.to_str().ok())
                    .and_then(|auth_value| auth_value.strip_prefix("Bearer "))
                    .ok_or_else(|| {
                        (
                            StatusCode::UNAUTHORIZED,
                            "Missing Authorization header with Bearer".to_string(),
                        )
                    })?;

                let mut validation = jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::RS256);
                validation.validate_aud = false;

                // Decode and verify JWT token with public key fetched from Authentik
                let token_data = jsonwebtoken::decode::<TokenClaims>(
                    &token,
                    &jwt_decoding_key,
                    &validation,
                )
                .map_err(|e| {
                    (
                        StatusCode::UNAUTHORIZED,
                        format!("Failed to verify and decode JWT. Confirm corret claims and signature: {:?}", e)
                    )
                })?;

                // Insert or update user on sub (subject, or id)
                sqlx::query!(
                    "
                    insert into users (id, username)
                    values ($1, $2)
                    on conflict (id) do update
                    set username = excluded.username
                    ",
                    token_data.claims.sub,
                    token_data.claims.name,
                )
                .execute(&db)
                .await
                .map_err(|_| {
                    (
                        StatusCode::INTERNAL_SERVER_ERROR,
                        "Unable to save user to database".to_string(),
                    )
                })?;

                // check if roles are correct
                let has_permissions = token_data.claims.groups.contains(&$perm);

                // Insset User and permissions as an Extension to be used later
                if has_permissions {
                    req.extensions_mut().insert(UserId {
                        id: token_data.claims.sub,
                        roles: token_data.claims.groups,
                    });

                    return Ok(next.run(req).await.into_response());
                } else {
                    return Err((
                        StatusCode::UNAUTHORIZED,
                        "Unauthorized to perform this action".to_string(),
                    ));
                }
            },
        )
    }};
}
