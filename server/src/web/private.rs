use axum::{routing::get, Router};

use crate::permission_required;

use super::app::KSPRPApp;

pub mod admin;
pub mod moderator;
pub mod protected;

pub fn router(state: KSPRPApp) -> Router<()> {
    Router::new()
        // Should just require user perimssion
        .route("/protected", get(protected::handler))
        // Requried moderator permission
        .merge(
            Router::new()
                .route("/moderator", get(moderator::handler))
                .route_layer(permission_required!(state.clone(), Permission::Moderator)),
        )
        // Requires admin permission
        .nest(
            "/admin",
            admin::router(state.clone())
                .route_layer(permission_required!(state.clone(), Permission::Admin)),
        )
}
