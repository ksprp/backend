use axum::{routing::post, Router};

use crate::web::app::KSPRPApp;

pub mod paint;

pub fn router(state: KSPRPApp) -> Router<()> {
    Router::new()
        .route("/paint", post(paint::handler))
        .with_state(state)
}
