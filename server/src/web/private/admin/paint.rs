use axum::extract::State;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::{debug_handler, extract};

use crate::game::map::{update_map, HexUpdate};
use crate::web::app::KSPRPApp;

/// Update a list of hexes, matching on id, can set the value of nation_id and or biome
/// If you send a hex update with a id that does not exist it will be ignored
/// Setting a nation id which does not exist will lead to a error
#[utoipa::path(
  post,
  path = "/private/admin/paint",
  request_body = Vec<HexUpdate>,
  responses(
    (status = 200, description = "Update succesful"),
    (status = 500, description = "Internal Server Error", body = String)
  )
)]
#[debug_handler]
pub async fn handler(
    State(KSPRPApp { db, .. }): State<KSPRPApp>,
    extract::Json(hex_updates): extract::Json<Vec<HexUpdate>>,
) -> impl IntoResponse {
    update_map(db, hex_updates)
        .await
        .map_err(|_| {
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                "Failed to update hexes {:?}}",
            )
        })
        .into_response()
}
