use axum::{routing::get, Router};

use super::app::KSPRPApp;

pub mod map;
pub mod time;

pub fn router(state: KSPRPApp) -> Router<()> {
    Router::new()
        .route("/time", get(time::handler))
        .route("/map", get(map::handler))
        .with_state(state)
}
