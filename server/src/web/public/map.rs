use axum::{extract::State, Json};

use crate::database::map::get_map;
use crate::game::map::Hex;
use crate::web::app::KSPRPApp;
use crate::web::utils::{internal_error, Return};

/// Returns information about all the map hexes
#[utoipa::path(
  get,
  path = "/public/map",
  responses(
    (status = 200, description = "An array containing information about all the hexes", body = Vec<Hex>),
    (status = 500, description = "Internal Server Error", body = String)
  )
)]
pub async fn handler(State(KSPRPApp { db, .. }): State<KSPRPApp>) -> Return<Json<Vec<Hex>>> {
    let mut conn = db.acquire().await.map_err(internal_error)?;
    let map = get_map(&mut conn).await.map_err(internal_error)?;
    Ok(Json(map))
}
