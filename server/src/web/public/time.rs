use axum::{extract::State, Json};

use crate::database;
use crate::game::time::Time;
use crate::web;
use crate::web::app::KSPRPApp;
use crate::web::utils::{internal_error, Return};

/// Returns the current game time
#[utoipa::path(
  get,
  path = "/public/time",
  responses(
    (status = 200, description = "List the current year and hour", body = Time),
    (status = 500, description = "Internal Server Error", body = String)
  )
)]
pub async fn handler(State(KSPRPApp { db, .. }): State<KSPRPApp>) -> Return<Json<Time>> {
    let mut conn = db.acquire().await.map_err(internal_error)?;
    let year: i32 = database::time::get_year(&mut *conn)
        .await
        .map_err(web::utils::internal_error)?;
    let hour: i32 = database::time::get_hour(&mut *conn)
        .await
        .map_err(web::utils::internal_error)?;
    Ok(Json(Time { year, hour }))
}
