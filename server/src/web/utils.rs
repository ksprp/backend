use axum::http::StatusCode;

/// Shorthand for common return type in the server module
pub type Return<T> = Result<T, (StatusCode, String)>;

/// Utility function for mapping any error into a `500 Internal Server Error`
/// response.
pub fn internal_error<E>(err: E) -> (StatusCode, String)
where
    E: std::error::Error,
{
    (StatusCode::INTERNAL_SERVER_ERROR, err.to_string())
}
